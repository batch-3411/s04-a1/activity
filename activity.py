from abc import ABC, abstractmethod

class Animal(ABC):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
    @abstractmethod
    def eat(self, food):
        pass
    
    @abstractmethod
    def make_sound(self):
        pass

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__(name, breed, age)
        
    def eat(self, food):
        print(f"Eaten {food}")
    
    def make_sound(self):
        print(f"Bark! Woof! Arf!")
        
    def call(self):
        print(f"Here {self._name}")

class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__(name, breed, age)
        
    def eat(self, food):
        print(f"Serve me {food}")
    
    def make_sound(self):
        print(f"Miaow! Nyaw! Nyaaaaa!")
        
    def call(self):
        print(f"{self._name}, come on!")

# Creating instances of Cat and Dog
dog = Dog("Isis", "Golden Retriever", 2)
cat = Cat("Puss", "Siamese", 3)

# Using methods
dog.eat("Steak")
dog.make_sound()
dog.call()

cat.eat("Tuna")
cat.make_sound()
cat.call()


